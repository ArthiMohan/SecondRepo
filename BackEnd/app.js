
const express = require('express')
const bodyParser = require('body-parser');
const app = express()
//const fs = require('fs');

var config=require('./config');

var FrontEndPath= __dirname + '/../frontEnd/';
app.use(express.static(FrontEndPath));//middle-ware


app.get('/home',(req,res)=>{
    res.redirect('index.html');
})

app.get('*',(req,res)=>{
    res.redirect('404error.html');  
})
app.post('/login', (req, res) => {
    const userDetails = {
        name: req.body.name,
        password: req.body.password
    }
    console.log(userDetails);
    connection.query('INSERT INTO login SET?', userDetails, (err, result) => {
        if (err) {
            if(err.errno == 1062){
                res.send({
                    msg: 'Duplicate value found,try again with proper values.'
                })
            }else{
                res.send(err);
            }
        } else {
            console.log(result);
            res.redirect('teamDetails.html');  
        }
    })
})
app.get('/', (req, res) => {
   console.log('this is / route');
})

app.listen(3000, (err,res) => {
    if(err){
        console.log(err);
    }else{
        console.log('Example app listening on ${config.port}')
    }
})